package com.flink.examples.service;

/**
 * @Description
 * @Author JL
 * @Date 2020/10/16
 * @Version V1.0
 */
public enum QuotaEnum {
    /**
     * 商品类型，品牌，性别, 用户
     */
    DEFAULT, GOODS_TYPE, BRAND, GENDER, USER

}
