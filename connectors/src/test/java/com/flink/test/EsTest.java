package com.flink.test;

import com.flink.examples.TUser;
import com.google.gson.Gson;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.Map;

/**
 * @Description
 * @Author JL
 * @Date 2021/01/21
 * @Version V1.0
 */
public class EsTest {

    public static void main(String[] args) throws Exception {
        RestClientBuilder builder = RestClient.builder(new HttpHost("192.168.110.35", 9200, "http"));

        Settings settings = Settings.builder().put("cluster.name", "my-application").build();
        Gson gson = new Gson();
        RestHighLevelClient client = null;
        //匹配查询
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchQuery("sex", 1));
        //定义索引库
        SearchRequest request = new SearchRequest();
        request.types("doc");
        request.indices("flink_demo");
        request.source(sourceBuilder);
        try {
            client = new RestHighLevelClient(builder);
            SearchResponse response = client.search(request);
//                    SearchResponse response = client.search(request, new Header[]{});
            SearchHits hits = response.getHits();
            System.out.println("查询结果有" + hits.getTotalHits() + "条");
            for (SearchHit searchHits : hits ) {
                Map<String,Object> dataMap = searchHits.getSourceAsMap();
                TUser user = gson.fromJson(gson.toJson(dataMap), TUser.class);
            }
        }catch(IOException ioe){
            ioe.printStackTrace();
        }finally {
            if (client != null){
                client.close();
            }
        }
    }

}
