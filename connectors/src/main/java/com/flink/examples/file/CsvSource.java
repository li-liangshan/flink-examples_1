package com.flink.examples.file;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple7;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description 从csv文件中读取内容输出到DataSet中
 * @Author JL
 * @Date 2020/09/19
 * @Version V1.0
 */
public class CsvSource {

    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String filePath = "D:\\Workspaces\\idea_2\\flink-examples\\connectors\\src\\main\\resources\\user.csv";
        DataSet<Tuple7<Integer, String, Integer, Integer, String, String, Long>> dataSet = env
                .readCsvFile(filePath)
                .fieldDelimiter(",")
                .types(Integer.class, String.class, Integer.class, Integer.class, String.class, String.class, Long.class);
        //打印
        dataSet.print();
    }

}
