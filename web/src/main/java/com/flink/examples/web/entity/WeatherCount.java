package com.flink.examples.web.entity;

import lombok.Data;

/**
 * @Description
 * @Author JL
 * @Date 2021/03/23
 * @Version V1.0
 */
@Data
public class WeatherCount implements java.io.Serializable {

    private String timeStr;
    private String city;
    private Integer siteCount;
    private Integer avgAir;
    private Double maxTemperature;
    private Integer avgHumidity;
    private Integer avgWindLevel;

}
