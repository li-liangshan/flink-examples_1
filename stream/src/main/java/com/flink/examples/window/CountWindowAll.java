package com.flink.examples.window;

import com.flink.examples.DataSource;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.List;

/**
 * @Description 不分区数量滚动窗口
 * @Author JL
 * @Date 2020/09/14
 * @Version V1.0
 */
public class CountWindowAll {
    /*
    窗口在处理流数据时，通常会对流进行分区；
    数据流划分为：
    keyed（根据key划分不同数据流区）
    non-keyed(指没有按key划分的数据流区，指所有原始数据流)
    */
    /**
     * 遍历集合，按数量窗口滚动，返回窗口下年龄的总和
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        List<Tuple3<String, String, Integer>> tuple3List = DataSource.getTuple3ToList();
        DataStream<Integer> dataStream = env.fromCollection(tuple3List)
                .map(new MapFunction<Tuple3<String, String, Integer>, Integer>() {
                    @Override
                    public Integer map(Tuple3<String, String, Integer> tuple3) throws Exception {
                        return tuple3.f2;
                    }
                })
                .returns(Types.INT)
                //按数量窗口滚动，每3个输入数据流，计算一次
                .countWindowAll(3)
                .sum(0);
        dataStream.print();
        env.execute("flink CountWindowAll job");
    }
}
/*
1> 70
2> 83
*/
