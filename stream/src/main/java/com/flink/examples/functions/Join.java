package com.flink.examples.functions;

import com.flink.examples.DataSource;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatJoinFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

/**
 * @Description Join算子：两个数据流通过内部相同的key分区，将窗口内两个数据流相同key数据元素计算后，合并输出（类似于mysql表的inner join操作）
 * @Author JL
 * @Date 2020/09/16
 * @Version V1.0
 */
public class Join {

    /**
     * Flink支持了两种Join：Window Join（窗口连接）和Interval Join（时间间隔连接），本示例演示的为Window Join
     * 官方文档：https://ci.apache.org/projects/flink/flink-docs-release-1.11/zh/dev/stream/operators/joining.html
     */

    /**
     * 两个数据流集合，对相同key进行内联，分配到同一个窗口下，合并并打印
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(4);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
//        //watermark 自动添加水印调度时间
//        env.getConfig().setAutoWatermarkInterval(200);

        List<Tuple3<String, String, Integer>> tuple3List1 = DataSource.getTuple3ToList();
        List<Tuple3<String, String, Integer>> tuple3List2 = Arrays.asList(
                new Tuple3<>("伍七", "girl", 18),
                new Tuple3<>("吴八", "man", 30)
        );

        //Datastream 1
        DataStream<Tuple3<String, String, Integer>> dataStream1 = env.fromCollection(tuple3List1)
                //添加水印窗口,如果不添加，则时间窗口会一直等待水印事件时间，不会执行apply
                .assignTimestampsAndWatermarks(WatermarkStrategy.<Tuple3<String, String, Integer>>forBoundedOutOfOrderness(Duration.ofSeconds(2))
                        .withTimestampAssigner((element, timestamp)->System.currentTimeMillis()));

        //Datastream 2
        DataStream<Tuple3<String, String, Integer>> dataStream2 = env.fromCollection(tuple3List2)
                //添加水印窗口,如果不添加，则时间窗口会一直等待水印事件时间，不会执行apply
                .assignTimestampsAndWatermarks(WatermarkStrategy.<Tuple3<String, String, Integer>>forBoundedOutOfOrderness(Duration.ofSeconds(2))
                        .withTimestampAssigner(new SerializableTimestampAssigner<Tuple3<String, String, Integer>>() {
                            @Override
                            public long extractTimestamp(Tuple3<String, String, Integer> element, long timestamp) {
                                return System.currentTimeMillis();
                            }
                        }));

        //Datastream 3
        DataStream<String> newDataStream = dataStream1.join(dataStream2)
                .where(new KeySelector<Tuple3<String, String, Integer>, String>() {
                    @Override
                    public String getKey(Tuple3<String, String, Integer> value) throws Exception {
                        System.out.println("first name:" + value.f0 + ",sex:" + value.f1);
                        return value.f1;
                    }
                })
                .equalTo(new KeySelector<Tuple3<String, String, Integer>, String>() {
                    @Override
                    public String getKey(Tuple3<String, String, Integer> value) throws Exception {
                        System.out.println("second name:" + value.f0 + ",sex:" + value.f1);
                        return value.f1;
                    }
                })
                .window(TumblingEventTimeWindows.of(Time.seconds(1)))
//                .apply(new JoinFunction<Tuple3<String, String, Integer>,Tuple3<String, String, Integer>,String>(){
//                    @Override
//                    public String join(Tuple3<String, String, Integer> first, Tuple3<String, String, Integer> second) throws Exception {
//                        return "first.f0="+first.f0 + "|second.f0" + second.f0;
//                    }
//                })
                .apply(new FlatJoinFunction<Tuple3<String, String, Integer>, Tuple3<String, String, Integer>, String>() {
                    @Override
                    public void join(Tuple3<String, String, Integer> first, Tuple3<String, String, Integer> second, Collector<String> out) throws Exception {
                        out.collect(first.f0 + "|" + first.f1 + "|" + first.f2 + "|" + second.f0 + "|" + second.f1 + "|" + second.f2);
                    }
                })
            ;
        newDataStream.print();
        env.execute("flink Join job");
    }
}
/*
4> 李四|girl|24|伍七|girl|18
4> 刘六|girl|32|伍七|girl|18
4> 伍七|girl|18|伍七|girl|18
2> 张三|man|20|吴八|man|30
2> 王五|man|29|吴八|man|30
2> 吴八|man|30|吴八|man|30
 */