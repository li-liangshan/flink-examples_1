package com.flink.examples.functions;

import com.flink.examples.DataSource;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.List;

/**
 * @Description Union算子：将多个数据流合并成一个新的数据流（数据类型必需一致）
 * @Author JL
 * @Date 2020/09/16
 * @Version V1.0
 */
public class Union {

    /**
     * 遍历集合，合并多个流并打印
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        List<Tuple3<String,String,Integer>> tuple3List = DataSource.getTuple3ToList();
        //Datastream 1
        DataStream<Tuple3<String,String,Integer>> dataStream1 = env.fromCollection(tuple3List);
        //Datastream 2
        DataStream<Tuple3<String,String,Integer>> dataStream2 = env.fromCollection(tuple3List);
        //Datastream 3
        DataStream<Tuple3<String,String,Integer>> dataStream = dataStream1.union(dataStream2);
        dataStream.print();
        env.execute("flink Union job");
    }

}
/*
3> (王五,man,29)
2> (李四,girl,24)
3> (王五,man,29)
1> (张三,man,20)
1> (张三,man,20)
1> (伍七,girl,18)
1> (伍七,girl,18)
2> (吴八,man,30)
2> (李四,girl,24)
2> (吴八,man,30)
4> (刘六,girl,32)
4> (刘六,girl,32)
 */