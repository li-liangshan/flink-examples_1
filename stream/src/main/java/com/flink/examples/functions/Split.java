package com.flink.examples.functions;

import com.flink.examples.DataSource;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.streaming.api.collector.selector.OutputSelector;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SplitStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description Split算子：将数据流切分成多个数据流（已过时，并且不能二次切分，不建议使用）
 * @Author JL
 * @Date 2020/09/16
 * @Version V1.0
 */
public class Split {

    /**
     * 参考示例：https://blog.csdn.net/jerome520zl/article/details/103963549
     */

    /**
     * 遍历集合，将数据流切分成多个流并打印
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        List<Tuple3<String, String, Integer>> tuple3List = DataSource.getTuple3ToList();
        //Datastream
        DataStream<Tuple3<String, String, Integer>> dataStream = env.fromCollection(tuple3List);
        //按性别进行拆分
        //flink.1.11.1显示SplitStream类过时，推荐用keyBy的方式进行窗口处理或SideOutput侧输出流处理；注意，使用split切分后的流，不可二次切分，否则会抛异常
        SplitStream<Tuple3<String, String, Integer>> split = dataStream.split(new OutputSelector<Tuple3<String, String, Integer>>() {
            @Override
            public Iterable<String> select(Tuple3<String, String, Integer> value) {
                List<String> output = new ArrayList<String>();
                if (value.f1.equals("man")) {
                    output.add("man");
                } else {
                    output.add("girl");
                }
                return output;
            }
        });

        //查询指定名称的数据流
        DataStream<Tuple4<String, String, Integer, String>> dataStream1 = split.select("man")
                .map(new MapFunction<Tuple3<String, String, Integer>, Tuple4<String, String, Integer, String>>() {
                    @Override
                    public Tuple4<String, String, Integer, String> map(Tuple3<String, String, Integer> t3) throws Exception {
                        return Tuple4.of(t3.f0, t3.f1, t3.f2, "男");
                    }
                });

        DataStream<Tuple4<String, String, Integer, String>> dataStream2 = split.select("girl")
                .map(new MapFunction<Tuple3<String, String, Integer>, Tuple4<String, String, Integer, String>>() {
                    @Override
                    public Tuple4<String, String, Integer, String> map(Tuple3<String, String, Integer> t3) throws Exception {
                        return Tuple4.of(t3.f0, t3.f1, t3.f2, "女");
                    }
                });
        //打印:男
        dataStream1.print();
        //打印：女
        dataStream2.print();

        env.execute("flink Split job");
    }

/*
(张三,man,20,男)
(李四,girl,24,女)
(王五,man,29,男)
(刘六,girl,32,女)
(伍七,girl,18,女)
(吴八,man,30,男)
 */

}
