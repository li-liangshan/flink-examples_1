package com.flink.examples.functions;

import com.flink.examples.DataSource;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

import java.util.List;

/**
 * @Description FlatMap算子：将数据流一行按逻辑或规则拆分成0行或多行输出
 * @Author JL
 * @Date 2020/09/14
 * @Version V1.0
 */
public class FlatMap {

    /**
     * 遍历集合，打印数据流中每一条记录中的所有字段
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        List<Tuple3<String,String,Integer>> tuple3List = DataSource.getTuple3ToList();
        DataStream<String> dataStream = env.fromCollection(tuple3List).flatMap(new FlatMapFunction<Tuple3<String,String,Integer>,String>() {
            @Override
            public void flatMap(Tuple3<String, String, Integer> tuple3, Collector<String> out) throws Exception {
                out.collect(tuple3.f0);
                out.collect(tuple3.f1);
                out.collect(tuple3.f2 + "");
            }
        });
        dataStream.print();
        env.execute("flink FlatMap job");
    }
}

/*
4> 刘六
4> girl
4> 32
1> 张三
1> man
1> 20
1> 伍七
1> girl
1> 18
2> 李四
2> girl
2> 24
2> 吴八
2> man
2> 30
3> 王五
3> man
3> 29
 */