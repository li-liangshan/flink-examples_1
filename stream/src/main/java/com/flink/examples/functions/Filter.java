package com.flink.examples.functions;

import com.flink.examples.DataSource;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.List;

/**
 * @Description Filter算子：对数据流进行过滤，只返回为true的数据
 * @Author JL
 * @Date 2020/09/14
 * @Version V1.0
 */
public class Filter {
    /**
     * 遍历集合，只打印性别为男性的记录
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        List<Tuple3<String,String,Integer>> tuple3List = DataSource.getTuple3ToList();
        DataStream<Tuple3<String,String,Integer>> dataStream = env.fromCollection(tuple3List).filter(new FilterFunction<Tuple3<String,String,Integer>>() {
            @Override
            public boolean filter(Tuple3<String, String, Integer> tuple3) throws Exception {
                //性别（1男，2女）
                return tuple3.f1.equals("man");
            }
        });
        dataStream.print();
        env.execute("flink Filter job");
    }
}

/*
4> (吴八,man,30)
3> (张三,man,20)
1> (王五,man,29)
 */